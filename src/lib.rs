//! # `all_is_bytes`
//!
//! [![crates.io](https://img.shields.io/crates/v/all-is-bytes.svg)](https://crates.io/crates/all-is-bytes)
//! [![Dependency Status](https://deps.rs/crate/all-is-bytes/0.1.0/status.svg)](https://deps.rs/crate/all-is-bytes/0.1.0)
//! [![License](https://img.shields.io/badge/license-Unlicense-blue.svg)](https://unlicense.org/)
//! [![MSRV](https://img.shields.io/badge/MSRV-1.38-white.svg)](https://doc.rust-lang.org/cargo/reference/manifest.html#the-rust-version-field)
//!
//! Casts anything to a slice of maybe-uninit bytes.
//!
//! # Disclaimer
//!
//! Please do not use this crate for any serious purpose. The API is unwieldy
//! and difficult to use soundly. [bytemuck] and [zerocopy] both provide a safe,
//! sound, and useful interface for casting between byte slices and more. This
//! crate is simply a silly demonstration of a concept.
//!
//! [bytemuck]: https://crates.io/crates/bytemuck
//! [zerocopy]: https://crates.io/crates/zerocopy

// Attributes
#![cfg_attr(not(any(doc, test)), no_std)]
// Lints
#![allow(clippy::inline_always)]
#![warn(clippy::missing_docs_in_private_items, missing_docs)]
#![deny(
	clippy::missing_safety_doc,
	clippy::undocumented_unsafe_blocks,
	unsafe_op_in_unsafe_fn
)]

use core::{
	mem::{self, MaybeUninit},
	slice,
};

/// Reinterprets as a byte slice.
///
/// Returns <code>&\[[`MaybeUninit`]\<u8\>\]</code> rather than `&[u8]` as it is
/// [undefined behavior] to read uninitialized memory, including padding bytes.
/// If the type does not contain any uninitialized memory, it is safe to cast it
/// to `&[u8]` yourself.
///
/// # Safety
///
/// The type must not have interior mutability.
///
/// In other words, the type cannot contain [`Cell`], [`UnsafeCell`], and the
/// like.
///
/// ## Why?
///
/// Aliasing. See here:
///
/// ```no_run
/// # use core::cell::Cell;
/// let cell = Cell::new(1u8);
/// let raw = unsafe { all_is_bytes::cast(&cell) };
/// cell.set(0u8); // Erm... well that just happened...
/// assert_eq!(cell.get(), unsafe { raw[0].assume_init() }); // Uh oh!
/// ```
///
/// This causes [undefined behavior].
///
/// [`Cell`]: core::cell::Cell
/// [`UnsafeCell`]: core::cell::UnsafeCell
/// [undefined behavior]: https://doc.rust-lang.org/stable/reference/behavior-considered-undefined.html
#[inline(always)]
#[must_use]
pub unsafe fn cast<T>(x: &T) -> &[MaybeUninit<u8>]
where
	T: ?Sized,
{
	let len = mem::size_of_val(x);
	let ptr = (x as *const T).cast::<MaybeUninit<u8>>();

	// SAFETY: `ptr` is valid and points to a value `len` bytes long.
	unsafe { slice::from_raw_parts(ptr, len) }
}

/// Reinterprets as a mutable byte slice.
///
/// Returns <code>&\[[`MaybeUninit`]\<u8\>\]</code> rather than `&[u8]` as it is
/// [undefined behavior] to read uninitialized memory, including padding bytes.
/// If the type does not contain any uninitialized memory, it is safe to cast it
/// to `&[u8]` yourself.
///
/// # Safety
///
/// All invariants of the type must be upheld.
///
/// ## Why?
///
/// See this example with [`NonZeroU8`]:
///
/// ```no_run
/// # use core::num::NonZeroU8;
/// let mut num = NonZeroU8::new(1).unwrap();
/// let raw = unsafe { all_is_bytes::cast_mut(&mut num) };
/// raw[0].write(0); // Don't do this!
/// ```
///
/// As `NonZero*` values must not be 0, this causes [undefined behavior].
///
/// References and [`NonNull`] values are similar in that they must not be null.
///
/// Additionally, [`str`] has the invariant that its contents must be valid
/// UTF-8. See [`str::as_bytes_mut`] for more information.
///
/// [`NonZeroU8`]: core::num::NonZeroU8
/// [`NonNull`]: core::ptr::NonNull
/// [undefined behavior]: https://doc.rust-lang.org/stable/reference/behavior-considered-undefined.html
#[inline(always)]
#[must_use]
pub unsafe fn cast_mut<T>(x: &mut T) -> &mut [MaybeUninit<u8>]
where
	T: ?Sized,
{
	let len = mem::size_of_val(x);
	let ptr = (x as *mut T).cast::<MaybeUninit<u8>>();

	// SAFETY: `ptr` is valid and points to a value `len` bytes long.
	unsafe { slice::from_raw_parts_mut(ptr, len) }
}
