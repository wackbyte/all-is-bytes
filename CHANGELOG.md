# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog],
and this project adheres to [Semantic Versioning].

## [Unreleased]

### Changed

- Make the disclaimer more direct.

## [0.1.0]

Released on 2023-04-30.

### Added

- `cast` and `cast_mut`.
- The minimum supported Rust version is 1.38.

[Unreleased]: https://codeberg.org/wackbyte/all-is-bytes/compare/v0.1.0...trunk
[0.1.0]: https://codeberg.org/wackbyte/all-is-bytes/releases/tag/v0.1.0

[Keep a Changelog]: https://keepachangelog.com/en/1.1.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
