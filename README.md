# `all_is_bytes`

[![crates.io](https://img.shields.io/crates/v/all-is-bytes.svg)](https://crates.io/crates/all-is-bytes)
[![docs.rs](https://docs.rs/all-is-bytes/badge.svg)](https://docs.rs/all-is-bytes)
[![Dependency Status](https://deps.rs/repo/codeberg/wackbyte/all-is-bytes/status.svg)](https://deps.rs/repo/codeberg/wackbyte/all-is-bytes)
[![License](https://img.shields.io/badge/license-Unlicense-blue.svg)](https://unlicense.org/)
[![MSRV](https://img.shields.io/badge/MSRV-1.38-white.svg)](https://doc.rust-lang.org/cargo/reference/manifest.html#the-rust-version-field)

Casts anything to a slice of maybe-uninit bytes.
